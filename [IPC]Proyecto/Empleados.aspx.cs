﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace _IPC_Proyecto
{
    public partial class Empleados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string connectionString = @"Data Source=LAPTOP-HU6LP2DK; Initial Catalog = tiendaOnline; Integrated Security=True;";
                string consulta = "select empleado.nit,empleado.nombres,empleado.apellidos,fechaNacimiento,empleado.direccionDomicilio,empleado.telefonoDomicilio,empleado.numeroCelular,empleado.email,empleado.id_puesto,empleado.contrasena,empleado.nitSupervisor,empleado.nitGerente,puesto.puesto,metaMensual.meta from empleado Inner Join puesto on puesto.id_puesto = empleado.id_puesto Inner Join metaMensual on metaMensual.nitEmpleado = empleado.nit";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter(consulta, sqlCon);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);
                    repEmpleado.DataSource = dtbl;
                    repEmpleado.DataBind();
                    sqlCon.Close();
                }
            }
            
        }


        private void insertarEmpleado(string nit,string nombres,string apellidos,DateTime nacimiento,string direccion,string telefono,string celular,string email, string pass, string nitSupervisor,string nitGerente,int puesto)
        {
            string connectionString = @"Data Source=LAPTOP-HU6LP2DK; Initial Catalog = tiendaOnline; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO empleado (nit,nombres,apellidos,fechaNacimiento,direccionDomicilio,telefonoDomicilio,numeroCelular,email,contrasena,nitSupervisor,nitGerente,id_puesto) VALUES ('" + nit + "' , '" + nombres + "' , '" + apellidos + "' , '" + nacimiento + "' , '" + direccion + "' , '" + telefono + "' , '" + celular + "' , '" + email + "' , '" + pass + "' , '" + nitSupervisor + "' , '" + nitGerente + "' , " + puesto + ")" , sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon.Close();
            }
        }

        private void insertarMetaMensual(DateTime mes,string nit,int meta)
        {
            string connectionString = @"Data Source=LAPTOP-HU6LP2DK; Initial Catalog = tiendaOnline; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("insert into metaMensual values('"+mes+"','"+nit+"',"+meta+")", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon.Close();
            }
        }

        private void insertarProductosMeta(int meta,int producto)
        {
            string connectionString = @"Data Source=LAPTOP-HU6LP2DK; Initial Catalog = tiendaOnline; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("insert into productosMeta values("+meta+","+producto+")", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon.Close();
            }
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (FileUploadControl.HasFile)
            {
                try
                {


                   
                    string filename = Path.GetFileName(FileUploadControl.FileName);
                    FileUploadControl.SaveAs(Server.MapPath("~/") + filename);
                    string ruta = Server.MapPath("~/") + filename;

                    //Create the XmlDocument.  
                    XmlDocument doc = new XmlDocument();
                    doc.Load(ruta);
                    //Display all the Activities alone in the XML
                    
                    XmlNodeList nit = doc.GetElementsByTagName("NIT");
                    XmlNodeList nombres = doc.GetElementsByTagName("nombres");
                    XmlNodeList apellidos = doc.GetElementsByTagName("apellidos");
                    XmlNodeList nacimiento = doc.GetElementsByTagName("nacimiento");
                    XmlNodeList direccion = doc.GetElementsByTagName("direccion");
                    XmlNodeList telefono = doc.GetElementsByTagName("telefono");
                    XmlNodeList celular = doc.GetElementsByTagName("celular");
                    XmlNodeList email = doc.GetElementsByTagName("email");
                    XmlNodeList codigo_puesto = doc.GetElementsByTagName("codigo_puesto");
                    XmlNodeList codigo_jefe = doc.GetElementsByTagName("codigo_jefe");
                    XmlNodeList pass = doc.GetElementsByTagName("pass");
                    XmlNodeList nitEmpleado = doc.GetElementsByTagName("NIT_empleado");
                    XmlNodeList mesMeta = doc.GetElementsByTagName("mes_meta");
                    XmlNodeList codigoProducto = doc.GetElementsByTagName("codigo_producto");
                    XmlNodeList metaVenta = doc.GetElementsByTagName("meta_venta");

                    for (int i = 0; i < nit.Count; i++)
                    {

                        if (codigo_puesto[i].InnerXml == "1")
                        {
                            insertarEmpleado(nit[i].InnerXml, nombres[i].InnerXml, apellidos[i].InnerXml, Convert.ToDateTime(nacimiento[i].InnerXml), direccion[i].InnerXml, telefono[i].InnerXml, celular[i].InnerXml, email[i].InnerXml, pass[i].InnerXml, codigo_jefe[i].InnerXml,null, Int32.Parse(codigo_puesto[i].InnerXml));
                        }
                        else if (codigo_puesto[i].InnerXml == "2")
                        {
                            insertarEmpleado(nit[i].InnerXml, nombres[i].InnerXml, apellidos[i].InnerXml, Convert.ToDateTime(nacimiento[i].InnerXml), direccion[i].InnerXml, telefono[i].InnerXml, celular[i].InnerXml, email[i].InnerXml, pass[i].InnerXml, null, codigo_jefe[i].InnerXml, Int32.Parse(codigo_puesto[i].InnerXml));
                        }
                        else if (codigo_puesto[i].InnerXml == "3")
                        {
                            insertarEmpleado(nit[i].InnerXml, nombres[i].InnerXml, apellidos[i].InnerXml, Convert.ToDateTime(nacimiento[i].InnerXml), direccion[i].InnerXml, telefono[i].InnerXml, celular[i].InnerXml, email[i].InnerXml, pass[i].InnerXml, null, null, Int32.Parse(codigo_puesto[i].InnerXml));
                        }
                        else if (codigo_puesto[i].InnerXml == "4")
                        {
                            insertarEmpleado(nit[i].InnerXml, nombres[i].InnerXml, apellidos[i].InnerXml, Convert.ToDateTime(nacimiento[i].InnerXml), direccion[i].InnerXml, telefono[i].InnerXml, celular[i].InnerXml, email[i].InnerXml, pass[i].InnerXml, null, null, Int32.Parse(codigo_puesto[i].InnerXml));
                        }
                    }

                    

                    for (int i = 0; i < codigoProducto.Count; i++)
                    {

                        string connectionString = @"Data Source=LAPTOP-HU6LP2DK; Initial Catalog = tiendaOnline; Integrated Security=True;";
                        SqlConnection cn = new SqlConnection(connectionString);
                        
                            cn.Open();
                            using (SqlCommand command = new SqlCommand("select metaMensual.id from metaMensual where metaMensual.nitEmpleado = " + "'" + nitEmpleado[i].InnerXml + "'" , cn))
                            {

                                SqlDataReader reader = command.ExecuteReader();
                                while (reader.Read())
                                {
                                    string id = reader.GetString(0);
                                    insertarProductosMeta(Int32.Parse(id), Int32.Parse(codigoProducto[i].InnerXml));

                                }
                            }
                            cn.Close();
                        

                        
                    }

                    

                    StatusLabel.Text = "Estado: Archivo Cargado";

                }
                catch (Exception ex)
                {
                    StatusLabel.Text = "Estado: El Archivo no se pudo cargar. Error:  " + ex.Message;
                }
            }
        }


        private void get_productos(string nit)
        {
           
                string connectionString = @"Data Source=LAPTOP-HU6LP2DK; Initial Catalog = tiendaOnline; Integrated Security=True;";
                string consulta = "select metaMensual.mes,producto.nombre,productosLista.precio from metaMensual Inner join productosMeta on productosMeta.metaMensual = metaMensual.id inner join producto on producto.codigo = productosMeta.producto inner join productosLista on productosLista.codigoProducto = producto.codigo inner join listaPrecios on listaPrecios.codigo = productosLista.codigoLista inner join empleado on empleado.nit = metaMensual.nitEmpleado where listaPrecios.fechaFin >= metaMensual.mes and empleado.nit = " + "'" + nit + "'";

                using (SqlConnection sqlCon2 = new SqlConnection(connectionString))
                {
                    sqlCon2.Open();
                    SqlDataAdapter sqlDa2 = new SqlDataAdapter(consulta, sqlCon2);
                    DataTable dtbl2 = new DataTable();
                    sqlDa2.Fill(dtbl2);
                    repListaProductos.DataSource = dtbl2;
                    repListaProductos.DataBind();
                    sqlCon2.Close();
                }
            
            
            
        }
        

        protected void Button2_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            switch (btn.CommandName)
            {
                case "ThisBtnClick":
                    string nit = btn.CommandArgument.ToString();
                    get_productos(nit);
                    break;
                case "ThatBtnClick":
                    string nit2 = btn.CommandArgument.ToString();
                    get_productos(nit2);
                    break;
            }
        }
    }
}