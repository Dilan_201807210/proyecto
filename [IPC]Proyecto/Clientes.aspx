﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="_IPC_Proyecto.Clientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class ="col-md-3 text-center">
                <h2 class="mt-3">
                    Clientes
                </h2>
            </div>
            <div class="col-md-6">
                
            </div>
            <div class="col-md-3 text-center">
                <div class="form-group">
                    <h3>Seleccionar Archivo</h3>
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 text-center">
                <h1>Clientes</h1>
                <table class="table table-striped table-hover table-bordered text-center table-sm">
                  <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nit</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Fecha Nacimiento</th>
                            <th scope="col">Dirección Domicilio</th>
                            <th scope="col">Telefono Domicilio</th>
                            <th scope="col">Numero de Celular</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Ciudad</th>
                            <th scope="col">Departamento</th>
                            <th scope="col">Limite Credito</th>
                            <th scope="col">Dias Credito</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="repCliente" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><asp:Label ID="Label1" runat="server" Text='<%# Eval("nit") %>'></asp:Label></th>
                                    <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("nombres") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("apellidos") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("fechaNac") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label5" runat="server" Text='<%# Eval("direccionDom") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label6" runat="server" Text='<%# Eval("telefenoDom") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label7" runat="server" Text='<%# Eval("numeroCel") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label8" runat="server" Text='<%# Eval("email") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label9" runat="server" Text='<%# Eval("ciudad") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label10" runat="server" Text='<%# Eval("departamento") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label12" runat="server" Text='<%# Eval("montoMaximo") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label13" runat="server" Text='<%# Eval("dias") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <h1>Empresas</h1>
                <table class="table table-striped table-hover table-bordered text-center table-sm">
                  <thead class="thead-light">
                        <tr>
                            <th scope="col">Nit</th>
                            <th scope="col">Nombre Empresa</th>
                            <th scope="col">Fecha Creación</th>
                            <th scope="col">Dirección</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Numero de Celular</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Ciudad</th>
                            <th scope="col">Departamento</th>
                            <th scope="col">Limite Credito</th>
                            <th scope="col">Dias Credito</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="repEmpresa" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><asp:Label ID="Label1" runat="server" Text='<%# Eval("nit") %>'></asp:Label></th>
                                    <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("nombres") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("fechaNac") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label5" runat="server" Text='<%# Eval("direccionDom") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label6" runat="server" Text='<%# Eval("telefenoDom") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label7" runat="server" Text='<%# Eval("numeroCel") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label8" runat="server" Text='<%# Eval("email") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label9" runat="server" Text='<%# Eval("ciudad") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label10" runat="server" Text='<%# Eval("departamento") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label12" runat="server" Text='<%# Eval("montoMaximo") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label13" runat="server" Text='<%# Eval("dias") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
