﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Empleados.aspx.cs" Inherits="_IPC_Proyecto.Empleados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class ="col-md-3 text-center">
                <h2 class="mt-3">
                    Empleados
                </h2>
            </div>
            <div class="col-md-6">
                <asp:FileUpload id="FileUploadControl" runat="server" />
                <asp:Button ID="Button1" runat="server" Text="Cargar" OnClick="UploadButton_Click" class="btn btn-primary"/>
                <asp:Label runat="server" id="StatusLabel" text="Estado: " />
            </div>
            <div class="col-md-3 text-center">
                
                
                
                <asp:TextBox ID="TextBox1" runat="server" Height="106px" Width="227px"></asp:TextBox>
                
                
                
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <table class="table table-striped table-hover table-bordered text-center table-sm">
                  <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nit</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Fecha Nacimiento</th>
                            <th scope="col">Dirección Domicilio</th>
                            <th scope="col">Telefono Domicilio</th>
                            <th scope="col">Numero de Celular</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Contraseña</th>
                            <th scope="col">Nit Supervisor</th>
                            <th scope="col">Nit Gerente</th>
                            <th scope="col">Puesto</th>
                            <th scope="col">Meta</th>
                            <th scope="col">Productos Vendidos</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="repEmpleado" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><asp:Label ID="Label1" runat="server" Text='<%# Eval("nit") %>'></asp:Label></th>
                                    <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("nombres") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("apellidos") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("fechaNacimiento") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label5" runat="server" Text='<%# Eval("direccionDomicilio") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label6" runat="server" Text='<%# Eval("telefonoDomicilio") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label7" runat="server" Text='<%# Eval("numeroCelular") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label8" runat="server" Text='<%# Eval("email") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label10" runat="server" Text='<%# Eval("contrasena") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label11" runat="server" Text='<%# Eval("nitSupervisor") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label12" runat="server" Text='<%# Eval("nitGerente") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label13" runat="server" Text='<%# Eval("puesto") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label9" runat="server" Text='<%# Eval("meta") %>'></asp:Label></td>
                                    <td><asp:Button ID="Button2" runat="server" CommandArgument='<%# Eval("nit") %>' CommandName="ThisBtnClick" Text="Ver" class="btn btn-outline-dark" OnClick="Button2_Click"/></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-hover table-bordered text-center table-sm">
                  <thead class="thead-dark">
                        <tr>
                            <th scope="col">Fecha</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="repListaProductos" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><asp:Label ID="Label1" runat="server" Text='<%# Eval("mes") %>'></asp:Label></th>
                                    <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("nombre") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("precio") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>

    
                            
</asp:Content>
