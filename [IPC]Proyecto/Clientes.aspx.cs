﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _IPC_Proyecto
{
    public partial class Clientes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-HU6LP2DK; Initial Catalog = tiendaOnline; Integrated Security=True;";
            string consulta = "select cliente.nit,cliente.nombres,cliente.apellidos,cliente.fechaNac,cliente.direccionDom,cliente.telefenoDom,cliente.numeroCel,cliente.email,ciudad.nombre as ciudad,departamento.nombre as departamento,credito.montoMaximo,credito.dias from cliente Inner Join credito on credito.nitCliente = cliente.nit Inner Join ciudad on ciudad.idCiudad = cliente.idCiudad Inner Join departamento on departamento.idDepartamento = ciudad.idDepartamento where cliente.apellidos is not null";
            string consulta2 = "select cliente.nit,cliente.nombres,cliente.fechaNac,cliente.direccionDom,cliente.telefenoDom,cliente.numeroCel,cliente.email,ciudad.nombre as ciudad,departamento.nombre as departamento,credito.montoMaximo,credito.dias from cliente Inner Join ciudad on ciudad.idCiudad = cliente.idCiudad Inner Join departamento on departamento.idDepartamento = ciudad.idDepartamento Inner Join credito on credito.nitCliente = cliente.nit where cliente.apellidos is NULL";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(consulta, sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                repCliente.DataSource = dtbl;
                repCliente.DataBind();

                SqlDataAdapter sqlDa2 = new SqlDataAdapter(consulta2, sqlCon);
                DataTable dtb2 = new DataTable();
                sqlDa2.Fill(dtb2);
                repEmpresa.DataSource = dtb2;
                repEmpresa.DataBind();
                sqlCon.Close();
            }
        }
    }
}