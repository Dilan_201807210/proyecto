﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _IPC_Proyecto
{
    public partial class Login : System.Web.UI.Page
    {
        public static class user
        {
            public static string name;
            public static string pass;
        }
        
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-HU6LP2DK; Initial Catalog = tiendaOnline; Integrated Security=True;";
            SqlConnection cn = new SqlConnection(connectionString);
            try
            {
                cn.Open();
                using (SqlCommand command = new SqlCommand("SELECT * FROM administrador", cn))
                {
                    
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string name = reader.GetString(0);  
                        string pass = reader.GetString(1);

                        // Guardar usuario Registrado
                        user.name = name;
                        user.pass = pass;

                        if (name == TextBox1.Text && pass == TextBox2.Text)
                        {
                            Response.Redirect("Admin.aspx");
                        }
                        else if (name == TextBox1.Text && pass != TextBox2.Text)
                        {
                            Response.Write("<script>alert('Contraseña Incorrecta')</script>");
                        }
                        else if (name != TextBox1.Text && pass == TextBox2.Text)
                        {
                            Response.Write("<script>alert('Usuario Incorrecta')</script>");
                        }
                        else
                        {
                            Response.Write("<script>alert('Usuario y Contraseña Incorrecta')</script>");
                        }
                    }
                }
                cn.Close();
            }
            catch (Exception ex) { }
            finally { }
        }
    }
}