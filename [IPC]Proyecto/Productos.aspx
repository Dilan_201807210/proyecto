﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="_IPC_Proyecto.Productos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class ="col-md-3 text-center">
                <h2 class="mt-3">
                    Productos
                </h2>
            </div>
            <div class="col-md-6">
                <h3>Carga de Archivos</h3>
                <asp:FileUpload ID="FileUpload1" runat="server" />
            </div>
            <div class="col-md-3 text-center">
                
                
                
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-6">
                <h3>Productos&nbsp;&nbsp;&nbsp;
                </h3>
                <table class="table table-striped table-hover table-bordered text-center table-sm">
                  <thead class="thead-dark">
                        <tr>
                            <th scope="col">Codigo</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Existencia</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="repProductos" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><asp:Label ID="Label0" runat="server" Text='<%# Eval("codigo") %>'></asp:Label></th>
                                    <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("nombre") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("tipo") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("descripcion") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("existencia") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <h3>Lista de Precios</h3>
                <table class="table table-striped table-hover table-bordered text-center table-sm">
                  <thead class="thead-dark">
                        <tr>
                            <th scope="col">Codigo</th>
                            <th scope="col">Fecha Inicio</th>
                            <th scope="col">Fecha Fin</th>
                            <th scope="col">Productos</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="repLista" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><asp:Label ID="Label5" runat="server" Text='<%# Eval("codigo") %>'></asp:Label></th>
                                    <td><asp:Label ID="Label6" runat="server" Text='<%# Eval("fechaInicio") %>'></asp:Label></td>
                                    <td><asp:Label ID="Label7" runat="server" Text='<%# Eval("fechaFin") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
</asp:Content>
