﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _IPC_Proyecto
{
    public partial class Productos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-HU6LP2DK; Initial Catalog = tiendaOnline; Integrated Security=True;";
            string consulta = "select producto.codigo,producto.nombre,categoria.tipo,producto.descripcion,producto.existencia from producto Inner Join categoria on categoria.id = producto.id_categoria";
            string consulta2 = "select * from listaPrecios";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(consulta, sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                repProductos.DataSource = dtbl;
                repProductos.DataBind();

                SqlDataAdapter sqlDa2 = new SqlDataAdapter(consulta2, sqlCon);
                DataTable dtb2 = new DataTable();
                sqlDa2.Fill(dtb2);
                repLista.DataSource = dtb2;
                repLista.DataBind();
                sqlCon.Close();
            }
        }
    }
}